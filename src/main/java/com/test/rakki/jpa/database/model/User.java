package com.test.rakki.jpa.database.model;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "userinfo")
public class User {
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private long id;
    @Column(name = "updated_time", nullable = false, columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP")
    private Date updatedDate;
    @Column(name = "creation_time", nullable = false, columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP")
    private Date createdDate;
    @Column(name = "name", length = 100)
    private String name;
    @Column(name = "userName", length = 100)
    private String userName;
    @Column(name = "mobileNo", length = 20)
    private String mobileNo;
    @Column(name = "password", length = 100)
    private String password;
    @Column(name = "role", length = 2)
    private String role = ROLE.ROLE_USER.name();

    public User() {

    }

    public User(String name, String mobileNo) {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    enum ROLE {
        ROLE_USER(1),
        ROLE_ADMIN(2);
        int role;

        ROLE(int role) {
            this.role = role;
        }

    }
}

