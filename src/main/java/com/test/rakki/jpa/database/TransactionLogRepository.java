package com.test.rakki.jpa.database;

import com.test.rakki.jpa.database.model.TransactionLog;
import org.springframework.data.repository.CrudRepository;

public interface TransactionLogRepository extends CrudRepository<TransactionLog, Long> {
}