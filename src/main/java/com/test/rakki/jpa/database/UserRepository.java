package com.test.rakki.jpa.database;

import com.test.rakki.jpa.database.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

    User findByUserName(String userName);
}