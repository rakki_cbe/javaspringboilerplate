package com.test.rakki.jpa.database.model;


import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "transactionMaster")
public class TransactionMaster {
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private long id;
    @Column(name = "updated_time", nullable = false, columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP")
    private Date updatedDate;
    @Column(name = "creation_time", nullable = false, columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP")
    private Date createdDate;
    @ManyToOne
    @JoinColumn(name = "userId", nullable = false)
    private User user;
    @Column(name = "amount")
    private double amountInvolved;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "transactionMaster", cascade = CascadeType.ALL)
    private Set<TransactionLog> log = new HashSet<>();

    public double getAmountInvolved() {
        return amountInvolved;
    }

    public void setAmountInvolved(double amountInvolved) {
        this.amountInvolved = amountInvolved;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<TransactionLog> getLog() {
        return log;
    }

    public void setLog(Set<TransactionLog> log) {
        this.log = log;
    }

    public enum TRANSACTION_TYPE {
        PAID_BY_YOU(1),
        PAID_BY_OTHER(2),
        PAYMENT_SETTLES(3);
        int type;

        TRANSACTION_TYPE(int type) {
            this.type = type;
        }

        public int getType() {
            return type;
        }
    }
}
