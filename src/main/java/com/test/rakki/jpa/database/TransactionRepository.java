package com.test.rakki.jpa.database;

import com.test.rakki.jpa.database.model.TransactionMaster;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TransactionRepository extends CrudRepository<TransactionMaster, Long> {
    List<TransactionMaster> findByUserId(String userName);
}