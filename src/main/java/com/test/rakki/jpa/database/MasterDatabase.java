package com.test.rakki.jpa.database;

import org.springframework.beans.factory.annotation.Autowired;

public class MasterDatabase {
    @Autowired
    TransactionRepository trasactionMasterTable;
    @Autowired
    TransactionLogRepository transactionLogTable;
    @Autowired
    UserRepository userTable;

    public MasterDatabase() {
    }

    public TransactionRepository getTrasactionMasterTable() {
        return trasactionMasterTable;
    }

    public void setTrasactionMasterTable(TransactionRepository trasactionMasterTable) {
        this.trasactionMasterTable = trasactionMasterTable;
    }

    public TransactionLogRepository getTransactionLogTable() {
        return transactionLogTable;
    }

    public void setTransactionLogTable(TransactionLogRepository transactionLogTable) {
        this.transactionLogTable = transactionLogTable;
    }

    public UserRepository getUserTable() {
        return userTable;
    }

    public void setUserTable(UserRepository userTable) {
        this.userTable = userTable;
    }
}
