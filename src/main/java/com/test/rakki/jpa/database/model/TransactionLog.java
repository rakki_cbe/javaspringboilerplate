package com.test.rakki.jpa.database.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "transactionLog")
public class TransactionLog {
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private long id;
    @Column(name = "updated_time", nullable = false, columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP")
    private Date updatedDate;
    @Column(name = "creation_time", nullable = false, columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP")
    private Date createdDate;
    @JoinColumn(name = "transactionId")
    @ManyToOne()
    private TransactionMaster transactionMaster;
    @JoinColumn(name = "userId")
    @ManyToOne()
    private User user;
    @Column(name = "amountShared")
    private double amountShared;
    @Column(name = "type")
    private int type;


    public TransactionMaster getTransactionMasterId() {
        return transactionMaster;
    }

    public void setTransactionMasterId(TransactionMaster transactionMaster) {
        this.transactionMaster = transactionMaster;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUserId() {
        return user;
    }

    public void setUserId(User user) {
        this.user = user;
    }

    public double getAmountShared() {
        return amountShared;
    }

    public void setAmountShared(double amountShared) {
        this.amountShared = amountShared;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }


}
