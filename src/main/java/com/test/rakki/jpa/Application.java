package com.test.rakki.jpa;


import com.test.rakki.jpa.database.MasterDatabase;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@ComponentScan(basePackages = "com.test.rakki.jpa")
//This will search all class which has service and attach with this class
@PropertySource(value = {"classpath:application.properties"})
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    MasterDatabase getMasterDb() {
        return new MasterDatabase();
    }
    /* @Bean
   public CommandLineRunner initUser(MasterDatabase db){
        return(args -> {
            if(db.getUserTable().count()<=0){
            User user =new User();
            user.setName("rakki");
            user.setMobileNo("4568973945");
                db.getUserTable().save(user);
            }
            for (User user1: db.getUserTable().findAll()){
                System.out.println("user count ");
                TransactionMaster transactionMaster =new TransactionMaster();
                transactionMaster.setAmountInvolved(100);
                transactionMaster.setUser(user1);
                transactionMaster= db.getTrasactionMasterTable().save(transactionMaster);
                TransactionLog transactionLog =new TransactionLog();
                transactionLog.setAmountShared(100);
                transactionLog.setTransactionMasterId(transactionMaster);
                transactionLog.setType(TransactionMaster.TRANSACTION_TYPE.PAID_BY_YOU.getType());
                transactionLog.setUserId(user1);
                db.getTransactionLogTable().save(transactionLog);
            }
            for(TransactionMaster data:db.getTrasactionMasterTable().findAll()){
                System.out.println("Transaction entity count  "+data.getLog().size());
            }





        });
    }
*/
}