package com.test.rakki.jpa.security;

import com.test.rakki.jpa.database.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class MyAppUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository userInfoDAO;

    @Override
    public UserDetails loadUserByUsername(String userName)
            throws UsernameNotFoundException {
        com.test.rakki.jpa.database.model.User activeUserInfo = userInfoDAO.findByUserName(userName);
        GrantedAuthority authority = new SimpleGrantedAuthority(activeUserInfo.getRole() + "");
        return new User(activeUserInfo.getUserName(),
                activeUserInfo.getPassword(), Collections.singletonList(authority));

    }
}
