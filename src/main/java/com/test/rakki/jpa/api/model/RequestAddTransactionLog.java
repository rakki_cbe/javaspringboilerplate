package com.test.rakki.jpa.api.model;

import com.test.rakki.jpa.database.MasterDatabase;
import com.test.rakki.jpa.database.model.TransactionLog;
import com.test.rakki.jpa.database.model.TransactionMaster;
import com.test.rakki.jpa.database.model.User;

import java.util.Date;
import java.util.Optional;


public class RequestAddTransactionLog {
    private Date updatedDate;
    private Date createdDate;
    private TransactionMaster transactionMaster;
    private long userId;
    private double amountShared;
    private int type;


    public TransactionMaster getTransactionMasterId() {
        return transactionMaster;
    }

    public void setTransactionMasterId(TransactionMaster transactionMaster) {
        this.transactionMaster = transactionMaster;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long user) {
        this.userId = user;
    }

    public double getAmountShared() {
        return amountShared;
    }

    public void setAmountShared(double amountShared) {
        this.amountShared = amountShared;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public TransactionLog getJpaSupportedObect(MasterDatabase db) {
        TransactionLog log = new TransactionLog();
        Optional<User> user = db.getUserTable().findById(userId);
        if (user.isPresent())
            log.setUserId(user.get());
        log.setAmountShared(amountShared);
        log.setType(type);
        return log;

    }

}
