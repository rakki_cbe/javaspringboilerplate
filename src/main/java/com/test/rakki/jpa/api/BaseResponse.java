package com.test.rakki.jpa.api;

public class BaseResponse<T> {
    private String status;
    private Integer code;
    private T data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public void setSuccessMessage() {
        setStatus(CODE.SUCCESS.name());
        setCode(CODE.SUCCESS.code);
    }


    public void setFailureMessage(int code) {
        setStatus(CODE.UNEXPECTED_ERROR.name());
        setCode(code);
    }

    enum CODE {
        USER_ALREADY_PRESENT(201),
        SUCCESS(200),
        UNEXPECTED_ERROR(203),
        NO_RECORD_FOUND(204);
        int code;

        CODE(int i) {
            code = i;
        }
    }
}
