package com.test.rakki.jpa.api.model;


import com.test.rakki.jpa.database.MasterDatabase;
import com.test.rakki.jpa.database.model.TransactionLog;
import com.test.rakki.jpa.database.model.TransactionMaster;
import com.test.rakki.jpa.database.model.User;

import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;


public class RequestAddTransactionMaster {
    private Date updatedDate;
    private Date createdDate;
    private long userId;
    private double amountInvolved;
    private Set<RequestAddTransactionLog> log = new HashSet<>();

    public double getAmountInvolved() {
        return amountInvolved;
    }

    public void setAmountInvolved(double amountInvolved) {
        this.amountInvolved = amountInvolved;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Set<RequestAddTransactionLog> getLog() {
        return log;
    }

    public void setLog(Set<RequestAddTransactionLog> log) {
        this.log = log;
    }

    public TransactionMaster getJpaSupportedObect(MasterDatabase db) {
        TransactionMaster master = new TransactionMaster();
        Optional<User> user = db.getUserTable().findById(userId);
        if (user.isPresent())
            master.setUser(user.get());
        master.setAmountInvolved(amountInvolved);
        Set<TransactionLog> logs = new HashSet<>();
        for (RequestAddTransactionLog log : getLog()) {
            logs.add(log.getJpaSupportedObect(db));
        }
        master.setLog(logs);
        return master;

    }


}
