package com.test.rakki.jpa.api;


import com.test.rakki.jpa.api.model.RequestAddTransactionMaster;
import com.test.rakki.jpa.api.model.RequestDeleteTransactionMaster;
import com.test.rakki.jpa.database.MasterDatabase;
import com.test.rakki.jpa.database.model.TransactionLog;
import com.test.rakki.jpa.database.model.TransactionMaster;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Null;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
public class TransactionController {
    @Autowired
    MasterDatabase db;

    @RequestMapping(value = "transaction/get/{id}", method = RequestMethod.GET)
    public BaseResponse<TransactionMaster> getTransaction(@PathVariable("id") long id) {
        BaseResponse<TransactionMaster> response = new BaseResponse();
        response.setData(db.getTrasactionMasterTable().findById(id).get());
        response.setSuccessMessage();
        return response;
    }

    @RequestMapping(value = "transaction/get/all", method = RequestMethod.GET)
    public BaseResponse<List<TransactionMaster>> getAllTransaction(Authentication authentication) {
        BaseResponse<List<TransactionMaster>> response = new BaseResponse();
        response.setData(db.getTrasactionMasterTable().findByUserId(authentication.getName()));
        response.setSuccessMessage();
        return response;
    }

    @RequestMapping(value = "transaction/add", method = RequestMethod.PUT, consumes = {"application/json"})
    public BaseResponse<TransactionMaster> createTransaction(Authentication authentication, @RequestBody RequestAddTransactionMaster request) {
        TransactionMaster master = request.getJpaSupportedObect(db);
        BaseResponse<TransactionMaster> response = new BaseResponse();
        TransactionMaster transactionNew = db.getTrasactionMasterTable().save(master);
        Set<TransactionLog> logs = new LinkedHashSet<>();
        for (TransactionLog log : master.getLog()) {
            TransactionLog logNew = db.getTransactionLogTable().save(log);
            logNew.setTransactionMasterId(null);//It will create a infinite loop reference we are removing it
            logs.add(logNew);


        }
        if (transactionNew != null) {
            transactionNew.setLog(logs);
            response.setSuccessMessage();
            response.setData(transactionNew);

        } else {
            response.setFailureMessage(BaseResponse.CODE.UNEXPECTED_ERROR.code);
        }
        return response;

    }

    @RequestMapping(value = "transaction/delete", method = RequestMethod.DELETE, consumes = {"application/json"})
    public BaseResponse<Null> deleteTransaction(Authentication authentication, @RequestBody RequestDeleteTransactionMaster transactionMaster) {
        Optional<TransactionMaster> master = db.getTrasactionMasterTable().findById(transactionMaster.getTrasacationMasterId());
        BaseResponse<Null> response = new BaseResponse<>();
        if (master.isPresent()) {
            db.getTrasactionMasterTable().delete(master.get());
            for (TransactionLog log : master.get().getLog()) {
                db.getTransactionLogTable().delete(log);
            }

            response.setSuccessMessage();

        } else {
            response.setFailureMessage(BaseResponse.CODE.NO_RECORD_FOUND.code);
            response.setStatus(BaseResponse.CODE.NO_RECORD_FOUND.name());
        }


        return response;

    }

    @RequestMapping(value = "transaction/transaction_entry/delete/{transactionEntryId}", method = RequestMethod.DELETE, consumes = {"application/json"})
    public BaseResponse<Null> deleteTransactionEntry(Authentication authentication,
                                                     @PathVariable long transactionEntryId) {
        Optional<TransactionLog> log = db.getTransactionLogTable().findById(transactionEntryId);
        BaseResponse<Null> response = new BaseResponse<>();
        if (log.isPresent()) {
            db.getTransactionLogTable().delete(log.get());


            response.setSuccessMessage();

        } else {
            response.setFailureMessage(BaseResponse.CODE.NO_RECORD_FOUND.code);
            response.setStatus(BaseResponse.CODE.NO_RECORD_FOUND.name());
        }


        return response;

    }



}
